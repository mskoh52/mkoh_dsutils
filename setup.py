#!/usr/bin/env python

from distutils.core import setup

setup(name='mkoh_dsutils',
      version='0.1',
      description='Utilities for doing data science stuff',
      author='Matthew Koh',
      author_email='mskoh52@gmail.com',
      url='https://github.com/mskoh52/mkoh_dsutils',
      packages=['mkoh_dsutils'],
      )

