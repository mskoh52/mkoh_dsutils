# Base trainer class
class Trainer:
    def __init__(self, dataset, model_cls,
                 model_hyperparameters, trainer_parameters):
        self.set_trainer_parameters(trainer_parameters)
        self.dataset = data
        self.model = model_cls(**model_hyperparameters)
        self.log = Logger()
        self.batch_counter = 0

    def run_batch(self):
        loss, predictions = self.update(dataset.get(self.batch_size))
        self.batch_counter += 1

        if loss:
            self.log('Loss on batch #{}: {}'.format(self.batch_counter, loss))

        if predictions:
            for m in self.extra_metrics:
                self.log('Extra metrics: {}'.format(m(predictions)))

    @abc.abstractmethod
    def update(self, batch):
        # update parameters
        # compute and return loss
        pass

class PytorchAdamTrainer(Trainer):
    def __init__(self, dataset, model_cls, blahblah):
        super().__init__(blahblah)
        assert(issubclass(model_cls, PytorchModel))
        self._optim = torch.optim.Adam(blahblah)

    def update(self, batch):
        self.model.predict(batch)
        self._optim.step()

class TensorflowAdamTrainer(Trainer):
    def __init__(self, dataset, model_cls, blahblah):
        super().__init__(blahblah)
        assert(issubclass(model_cls, TensorflowModel))
        self._optimizer = tf.train.AdamOptimizer(blahblah)

    def update(self, batch):
        self.model.predict(batch)
        self._optim.minimize(self.model.loss_op)

# classes for data management
class Dataset:
    def train(self):
        return self._train
    def dev(self):
        return self._dev
    def test(self):
        return self._test

class DatasetPartition:
    def get(self, batch_size):
        return self._data[ix:ix+batch_size]

# model classes
class Model:
    def __init__(*args, **kwargs):
        pass

    @abc.abstractmethod
    def predict(self, data):
        pass

class PytorchModel(Model):
    def predict(self, data):
        pass

class TensorflowModel(Model):
    def __init__(self, layerspec):
        # set up the layers
        self._loss = tf.reduce_mean(tf.nn.softmax_with_logits(labels=labels,
                                                              logits=logits))

    def loss_op(self):
        return self._loss

    def predict(self, data):
        pass

class XgboostModel(Model):
    pass

class LibsvmModel(Model):
    # is this the same api as liblinear?
    pass
