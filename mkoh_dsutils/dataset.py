from .fixeddict import FixedDict


class View:
    def __init__(self, data):
        self.data = data
    def __len__(self):
        return len(self.data)
    def __getitem__(self, i):
        return self.data[i]


class Dataset:
    def __init__(self, name, data):
        self.name = name
        self.data = FixedDict.fromkeys('train', 'dev', 'test')
        if callable(data):
            self.data.update(data())
        else:
            self.data.update(data)
        self.data.lock()
        # TODO should there be some way to enforce that the contents of data
        # are immutable?

    @property
    def train(self):
        return View(self.data['train'])
    @property
    def dev(self):
        return View(self.data['dev'])
    @property
    def test(self):
        return View(self.data['test'])

    def __iadd__(self, other):
        self.name += '+' + other.name
        self.data['train'] += other.data['train']
        self.data['dev'] += other.data['dev']
        self.data['test'] += other.data['test']
        return self

    def __add__(self, other):
        name = self.name + '+' + other.name
        data = {}
        data['train'] = self.data['train'] + other.data['train']
        data['dev'] = self.data['dev'] + other.data['dev']
        data['test'] = self.data['test'] + other.data['test']
        return self.__class__(name, data)

    def __len__(self):
        return len(self.data['train']) + \
            len(self.data['dev']) + \
            len(self.data['test'])


