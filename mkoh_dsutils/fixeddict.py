class FixedDictError(Exception):
    pass

class FixedDict:
    @classmethod
    def fromkeys(cls, *args):
        return cls(dict.fromkeys(args))

    def __init__(self, dictionary):
        self._dict = dict(dictionary)
        self._locked = False

    def __setitem__(self, key, val):
        if self._locked:
            raise FixedDictError(
                'Dictionary at {} is locked'.format(hex(id(self))))

        if key not in self._dict:
            raise FixedDictError('Adding new key {} not allowed'.format(key))

        self._dict[key] = val

    def __getitem__(self, key):
        return self._dict[key]

    def __repr__(self):
        return repr(self._dict)

    def __str__(self):
        return str(self._dict)

    def update(self, other):
        if self._locked:
            raise FixedDictError(
                'FixedDict at {} is locked'.format(hex(id(self))))

        for k in other:
            if k not in self._dict:
                raise FixedDictError('Adding new key {} not allowed'.format(k))

        self._dict.update(other)

    def unlock(self):
        self._locked = False
    def lock(self):
        self._locked = True


