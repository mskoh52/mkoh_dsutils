class Indexer:
    def __init__(self, preserve_casing=False):
        self.dict = {}
        self.case = preserve_casing

    def __getitem__(self, x):
        try:
            if not self.case:
                x = x.lower()

            return self.dict[x]
        except KeyError:
            return 0

    def __setitem__(self, x, val):
        raise NotImplementedError

    def __len__(self):
        return len(self.dict)

    def insert(self, x):
        if not self.case:
            x = x.lower()
        if x not in self.dict:
            self.dict[x] = len(self.dict)+1
